use crate::db;
use rusqlite::params;

#[derive(Serialize, Deserialize, Debug)]
pub struct Entry {
    pub id: String,
    pub title: String,
    pub link: String,
    pub feed_id: String,
}

pub fn get_entry(entry_id: String) -> Option<Entry> {
    match db::open_connection() {
        Some(conn) => {
            let sql = "SELECT * FROM entry WHERE id = ?1 LIMIT 1";

            let mut stmt = conn.prepare(sql).unwrap();
            let entry_iter = stmt
                .query_map(params![entry_id], |row| Ok(entry(row)))
                .unwrap();

            let mut coll: Vec<Entry> = entry_iter.map(|f| f.unwrap()).collect();

            coll.pop()
        }
        _ => None,
    }
}

fn entry(row: &rusqlite::Row) -> Entry {
    Entry {
        id: row.get::<usize, String>(0).unwrap(),
        title: row.get::<usize, String>(1).unwrap(),
        link: row.get::<usize, String>(2).unwrap(),
        feed_id: row.get::<usize, String>(5).unwrap(),
    }
}
