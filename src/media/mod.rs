use crate::db;
use rusqlite::params;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Media {
    pub url: String,
    pub medium: String,
    pub feed_id: String,
    pub entry_id: String,
}

pub fn get_media(entry_id: String) -> Option<Vec<Media>> {
    match db::open_connection() {
        Some(conn) => {
            let sql = "SELECT * FROM media WHERE entryid = ?1";

            let mut stmt = conn.prepare(sql).unwrap();
            let media_iter = stmt
                .query_map(params![entry_id], |row| Ok(media(row)))
                .unwrap();

            Some(media_iter.map(|f| f.unwrap()).collect())
        }
        _ => None,
    }
}

fn media(row: &rusqlite::Row) -> Media {
    Media {
        url: row.get::<usize, String>(0).unwrap(),
        medium: row.get::<usize, String>(2).unwrap(),
        feed_id: row.get::<usize, String>(3).unwrap(),
        entry_id: row.get::<usize, String>(4).unwrap(),
    }
}

pub fn get_medium(media: Vec<Media>, medium: String) -> Option<Media> {
    if let Some(pos) = media.iter().position(|x| x.medium == medium) {
        if let Some(m) = media.get(pos) {
            return Some(m.clone());
        }
    }

    None
}
