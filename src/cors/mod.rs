use actix_cors::Cors;


pub fn cors() -> Cors {
    // Cors::default()
    //     .allowed_origin("http://pihole.lan/")
    //     .allowed_methods(vec!["GET", "POST", "DELETE"])
    //     .allowed_headers(vec![header::AUTHORIZATION, header::ACCEPT])
    //     .allowed_header(header::CONTENT_TYPE)
    //     .max_age(3600)
    //
    Cors::permissive()
}
