extern crate chrono;
extern crate serde_derive;
use crate::db;
use chrono::Utc;
use rusqlite::params;
use uuid::fmt::Hyphenated;
use uuid::Uuid;

const RETENTION_DAYS: i64 = 2;

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub enum FeedType {
    None = 0,
    Rss = 1,
    Atom = 2,
    Rfd = 3,
}

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub enum CleanBuffer {
    No = 0,
    Yes = 1,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Feed {
    pub name: String,
    pub id: String,
    pub link: String,
    pub feed_type: FeedType,
    pub clean_buffer: CleanBuffer,
    pub updated: String,
    pub last_run: String,
    pub tags: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct FeedRead {
    pub visited: String,
    pub feed_id: String,
    pub created: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Visited {
    pub visited: String,
    pub feed_id: String,
}

pub fn mark_visited(user_id: Hyphenated, visited: Visited) -> Option<bool> {
    match db::open_connection() {
        Some(conn) => {
            let sql = "INSERT INTO feeds_read(visited, feed_id,  user_id, created) VALUES(?1, ?2, ?3, ?4)";
            match conn.execute(
                sql,
                params![
                    visited.visited,
                    visited.feed_id,
                    user_id.to_string(),
                    Utc::now()
                ],
            ) {
                Ok(_) => Some(true),
                Err(e) => {
                    println!("{} - {}", Utc::now(), e);
                    None
                }
            }
        }
        _ => None,
    }
}

pub fn clean_feeds_read(user_id: Hyphenated) -> Option<bool> {
    match db::open_connection() {
        Some(conn) => {
            let sql = format!("DELETE FROM feeds_read WHERE user_id = ?1 and created < DATETIME('now', 'localtime', '0 month', '-{} day')", RETENTION_DAYS);

            match conn.execute(&sql, params![user_id.to_string()]) {
                Ok(_) => Some(true),
                Err(e) => {
                    println!("{} - {}", Utc::now(), e);
                    None
                }
            }
        }
        _ => None,
    }
}

pub fn get_read_by_user(user_id: Hyphenated) -> Vec<FeedRead> {
    match db::open_connection() {
        Some(conn) => {
            let sql = "SELECT visited, feed_id, created FROM feeds_read WHERE user_id = ?1;";
            let mut stmt = conn.prepare(sql).unwrap();

            let x = match stmt.query_map(params![user_id.to_string()], |row| {
                Ok(FeedRead {
                    visited: row.get::<usize, String>(0).unwrap(),
                    feed_id: row.get::<usize, String>(1).unwrap(),
                    created: row.get::<usize, String>(2).unwrap(),
                })
            }) {
                Ok(ri) => ri.map(|fr| fr.unwrap()).collect(),
                Err(e) => {
                    println!("{:#?}", e);
                    vec![]
                }
            };
            x
        }
        _ => vec![],
    }
}

pub fn get_feeds() -> Option<Vec<Feed>> {
    match db::open_connection() {
        Some(conn) => {
            let sql = "SELECT * FROM feed";

            let mut stmt = conn.prepare(sql).unwrap();
            let feeds_iter = stmt.query_map(params![], |row| Ok(feed(row))).unwrap();

            Some(feeds_iter.map(|feed| feed.unwrap()).collect())
        }
        _ => None,
    }
}

pub fn get_feed(feed_id: String) -> Option<Feed> {
    match db::open_connection() {
        Some(conn) => {
            let sql = "SELECT * FROM feed WHERE id = ?1 LIMIT 1";

            let mut stmt = conn.prepare(sql).unwrap();
            let feed_iter = stmt
                .query_map(params![feed_id], |row| Ok(feed(row)))
                .unwrap();

            let mut coll: Vec<Feed> = feed_iter.map(|f| f.unwrap()).collect();

            coll.pop()
        }
        _ => None,
    }
}

pub fn create_feed(feed: Feed) -> Option<String> {
    let feed_type = feed.feed_type as u32;
    let clean_buffer = feed.clean_buffer as u32;

    match db::open_connection() {
        Some(conn) => {
            let id = Uuid::new_v4();
            let id = id.hyphenated();
            let id = id.to_string();
            match conn.execute("INSERT INTO feed(id, title, updated, nextRun, feedType, link, cleanbuffer, tags) VALUES(?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)",
                               params![id, feed.name, Utc::now(), Utc::now(), feed_type, feed.link, clean_buffer, tags_to_string(feed.tags)]) {
                Ok(_) => Some(id.to_string()),
                Err(_) => None
            }
        }
        _ => None,
    }
}

pub fn delete_feed(feed_id: String) -> Option<bool> {
    match db::open_connection() {
        Some(conn) => {
            match conn.execute("DELETE FROM media WHERE feedid = ?1", params![feed_id]) {
                Ok(_) => {}
                Err(e) => {
                    println!("{} - {}", Utc::now(), e);
                }
            }

            match conn.execute("DELETE FROM entry WHERE feedid = ?1", params![feed_id]) {
                Ok(_) => {}
                Err(e) => {
                    println!("{} - {}", Utc::now(), e);
                }
            }

            match conn.execute("DELETE FROM feed WHERE id = ?1", params![feed_id]) {
                Ok(_) => {}
                Err(e) => {
                    println!("{} - {}", Utc::now(), e);
                }
            }

            Some(true)
        }
        _ => None,
    }
}

fn feed(row: &rusqlite::Row) -> Feed {
    Feed {
        id: row.get::<usize, String>(0).unwrap(),
        name: row.get::<usize, String>(1).unwrap(),
        link: row.get::<usize, String>(5).unwrap(),
        feed_type: match row.get::<usize, i32>(4).unwrap() {
            1 => FeedType::Rss,
            2 => FeedType::Atom,
            3 => FeedType::Rfd,
            _ => FeedType::None,
        },
        clean_buffer: match row.get::<usize, i32>(6).unwrap() {
            1 => CleanBuffer::Yes,
            _ => CleanBuffer::No,
        },
        updated: row.get::<usize, String>(2).unwrap(),
        last_run: row.get::<usize, String>(3).unwrap(),
        tags: get_tags(row.get::<usize, String>(7).unwrap()),
    }
}

fn get_tags(str_tags: String) -> Vec<String> {
    let mut tags: Vec<String> = Vec::new();
    let mut it = str_tags.chars().peekable();
    let mut tag = String::new();

    while let Some(&ch) = it.peek() {
        match ch {
            '#' => {
                if !tag.is_empty() {
                    tags.push(tag.trim().to_string());
                }
                tag = String::new();
                it.next().unwrap();
            }
            _ => {
                tag.push(ch);
                it.next().unwrap();
            }
        }
    }

    if !tag.is_empty() {
        tags.push(tag.trim().to_string());
    }

    tags
}

fn tags_to_string(tags: Vec<String>) -> String {
    let mut str_tags = String::new();
    for tag in &tags {
        str_tags.push('#');
        str_tags.push_str(tag);
        str_tags.push(' ');
    }

    str_tags
}
