use actix_web::{App, HttpServer};
use local_ip_address::local_ip;
use std::fs;
use std::io;

#[macro_use]
extern crate serde_derive;

mod cors;
mod db;
mod download;
mod entry;
mod feed;
mod handlers;
mod media;
mod routes;
mod tools;

#[actix_web::main]
async fn main() -> io::Result<()> {
    bootstrap();

    let port = 54321;
    let bind_address = local_ip().unwrap();
    let bind_address_lh = "127.0.0.1";
    let hs = HttpServer::new(|| {
        let cors = cors::cors();

        App::new().wrap(cors).configure(routes::routes)
    })
    .bind((bind_address, port))
    .unwrap_or_else(|_| panic!("Could not bind server to address {}", &bind_address))
    .bind((bind_address_lh, port))
    .unwrap_or_else(|_| panic!("Could not bind server to address {}", &bind_address_lh));

    hs.run().await
}

fn bootstrap() {
    let root = tools::get_root_folder();
    let dl = root.join("download");

    if let Ok(paths) = fs::read_dir(dl) {
        for entry in paths.flatten() {
            let path = entry.path();
            fs::remove_file(path).unwrap();
        }
    }
}
