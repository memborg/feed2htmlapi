use std::env;
use std::path::Path;
use std::path::PathBuf;

pub fn get_root_folder() -> PathBuf {
    let exe = env::current_exe().unwrap();
    exe.parent().unwrap().to_path_buf()
}

pub fn get_path_as_string(pb: &Path) -> String {
    pb.display().to_string()
}
