use crate::tools;
use chrono::Utc;
use rusqlite::Connection;
use std::path::PathBuf;

pub fn open_connection() -> Option<rusqlite::Connection> {
    if let Ok(conn) = Connection::open(get_file()) {
        pre_query(&conn);

        return Some(conn);
    }

    None
}

fn get_file() -> PathBuf {
    let root = tools::get_root_folder();
    let root = root.join("data");
    root.join("data.sqlite")
}

fn pre_query(conn: &Connection) {
    let sql = r#"
        pragma journal_mode = WAL;
        pragma synchronous = normal;
        pragma temp_store = memory;
        pragma mmap_size = 2147483648;
        "#;

    match conn.execute_batch(sql) {
        Ok(_) => {}
        Err(e) => {
            println!("pre_query {} - {}", Utc::now(), e);
        }
    }
}
