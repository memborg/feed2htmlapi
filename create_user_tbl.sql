CREATE TABLE users (
    id TEXT PRIMARY KEY NOT NULL,
    username TEXT UNIQUE NOT NULL,
    created TEXT NOT NULL
);

INSERT INTO users(id, username, created) VALUES('3c32e6dc-1dc4-4c8a-8201-9b6197739198','memborg','2023-07-26T17:35:08+00:00');

CREATE TABLE feeds_read (
    visited TEXT PRIMARY KEY  NOT NULL,
    feed_id TEXT NOT NULL,
    user_id TEXT NOT NULL,
    created TEXT NOT NULL
);
