use crate::entry;
use crate::feed;
use crate::media;
use crate::tools;
use regex::Regex;
use reqwest::header::HeaderMap;
use std::fs;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;
use std::path::PathBuf;
use std::process::Command;
use which::which;

const USER_AGENT: &str = "curl/7.88.1";

pub async fn download_entry(entry_id: String) -> Option<PathBuf> {
    if let Some(entry) = entry::get_entry(entry_id) {
        if let Some(feed) = feed::get_feed(entry.feed_id.clone()) {
            let tags = &feed.tags;

            if tags.contains(&String::from("youtube")) {
                if tags.contains(&String::from("podcast")) {
                    return download_youtube_audio(&entry, &feed);
                } else {
                    return download_youtube(&entry, &feed);
                }
            }

            if tags.contains(&String::from("podcast")) {
                return download_podcast(&entry, &feed).await;
            }
        }
    }

    None
}

fn download_youtube(entry: &entry::Entry, feed: &feed::Feed) -> Option<PathBuf> {
    let path = dowload_dir();
    let filename = format!("{} - {}.mkv", feed.name, entry.title);
    let filename = clean_filename(&filename);

    if let Some(yt) = get_exe("yt-dlp") {
        let output = Command::new(yt)
            .arg("-f")
            .arg("bv+ba/b")
            .arg("-S")
            .arg("res:1080")
            .arg("--windows-filenames")
            .arg("--embed-metadata")
            .arg("--embed-thumbnail")
            .arg("--convert-thumbnails")
            .arg("jpg")
            .arg("--embed-chapters")
            .arg("--sponsorblock-mark")
            .arg("all")
            .arg("--merge-output-format")
            .arg("mkv")
            .arg("--quiet")
            .arg("--no-warnings")
            .arg("--paths")
            .arg(path.as_path().display().to_string())
            .arg("--output")
            .arg(&filename)
            .arg(entry.link.clone())
            .output()
            .expect("Failed to execute process");

        if output.status.success() {
            let filename = path.join(filename);
            return Some(filename);
        } else {
            io::stdout().write_all(&output.stdout).unwrap();
            io::stderr().write_all(&output.stderr).unwrap();
        }
    } else {
        println!("Failed to load yt-dlp")
    }

    None
}

fn download_youtube_audio(entry: &entry::Entry, feed: &feed::Feed) -> Option<PathBuf> {
    let path = dowload_dir();
    let filename = format!("{} - {}.m4a", feed.name, entry.title);
    let filename = clean_filename(&filename);
    let filename = path.join(filename);
    if let Some(yt) = get_exe("yt-dlp") {
        let output = Command::new(yt)
            .arg("-f")
            .arg("m4a")
            .arg("--windows-filenames")
            .arg("--embed-metadata")
            .arg("--embed-thumbnail")
            .arg("--convert-thumbnails")
            .arg("jpg")
            .arg("--embed-chapters")
            .arg("--sponsorblock-remove")
            .arg("all")
            .arg("--quiet")
            .arg("--no-warnings")
            .arg("--output")
            .arg(&filename)
            .arg(entry.link.clone())
            .output()
            .expect("Failed to execute process");

        if output.status.success() {
            if let Some(tags_file) = write_tags(&filename, entry, feed) {
                if fs::rename(tags_file.clone(), filename.clone()).is_ok() {
                    return Some(filename);
                }
            }
        } else {
            io::stdout().write_all(&output.stdout).unwrap();
            io::stderr().write_all(&output.stderr).unwrap();
        }
    } else {
        println!("Failed to load yt-dlp")
    }

    None
}

async fn download_podcast(entry: &entry::Entry, feed: &feed::Feed) -> Option<PathBuf> {
    let path = dowload_dir();

    if let Some(media) = media::get_media(entry.id.clone()) {
        if let Some(media) = media::get_medium(media, String::from("audio/mpeg")) {
            let mut headers = HeaderMap::new();
            headers.insert(reqwest::header::USER_AGENT, USER_AGENT.parse().unwrap());

            if let Ok(uri) = reqwest::Url::parse(&media.url) {
                if let Some(remote_filename) = get_uri_filename(&uri) {
                    let filename = clean_filename(&remote_filename);
                    let filename = path.join(filename);
                    if let Some(ext) = get_file_ext(&filename) {
                        let client = reqwest::Client::new();
                        let response = client.get(uri).headers(headers);

                        match response.send().await {
                            Ok(resp) => {
                                if let Ok(body) = resp.bytes().await {
                                    if let Ok(mut file) = File::create(&filename) {
                                        file.write_all(&body).unwrap();
                                        if let Some(tags_file) = write_tags(&filename, entry, feed)
                                        {
                                            let filename =
                                                format!("{} - {}.{}", feed.name, entry.title, ext);
                                            let filename = clean_filename(&filename);
                                            let filename = path.join(filename);

                                            if fs::rename(tags_file.clone(), filename.clone())
                                                .is_ok()
                                            {
                                                if let Some(image_file) =
                                                    download_image(entry).await
                                                {
                                                    if let Some(cover_file) =
                                                        write_cover(&filename, &image_file)
                                                    {
                                                        if fs::rename(
                                                            cover_file.clone(),
                                                            filename.clone(),
                                                        )
                                                        .is_ok()
                                                        {
                                                            return Some(filename);
                                                        }
                                                    }
                                                }

                                                return Some(filename);
                                            }
                                        }
                                    }
                                }
                            }
                            Err(e) => {
                                println!("{:?}", e);
                            } //Failed to contact server
                        };
                    }
                }
            }
        }
    }

    None
}

async fn download_image(entry: &entry::Entry) -> Option<PathBuf> {
    let path = dowload_dir();
    if let Some(media) = media::get_media(entry.id.clone()) {
        if let Some(media) = media::get_medium(media, String::from("image")) {
            let mut headers = HeaderMap::new();
            headers.insert(reqwest::header::USER_AGENT, USER_AGENT.parse().unwrap());

            if let Ok(uri) = reqwest::Url::parse(&media.url) {
                if let Some(remote_filename) = get_uri_filename(&uri) {
                    let filename = clean_filename(&remote_filename);
                    let filename = path.join(filename);
                    let client = reqwest::Client::new();
                    let response = client.get(uri).headers(headers);

                    match response.send().await {
                        Ok(resp) => {
                            if let Ok(body) = resp.bytes().await {
                                if let Ok(mut file) = File::create(&filename) {
                                    file.write_all(&body).unwrap();

                                    return Some(filename);
                                }
                            }
                        }
                        Err(e) => {
                            println!("{:?}", e);
                        } //Failed to contact server
                    };
                }
            }
        }
    }

    None
}

fn write_cover(filename: &PathBuf, filename_image: &Path) -> Option<PathBuf> {
    if let Some(filename_no_ext) = get_filename_no_ext(filename) {
        if let Some(ext) = get_file_ext(filename) {
            let filename_cover = format!("{}-cover.{}", filename_no_ext, ext);

            let path = dowload_dir();
            let filename_cover = path.join(filename_cover);
            let output = Command::new(get_exe("ffmpeg").unwrap())
                .arg("-i")
                .arg(tools::get_path_as_string(filename))
                .arg("-i")
                .arg(tools::get_path_as_string(filename_image))
                .arg("-map")
                .arg("1")
                .arg("-map")
                .arg("0")
                .arg("-y") //Always overwrite file
                .arg("-codec")
                .arg("copy") // Copy only no reencoding happening
                .arg("-disposition:0") // Copy only no reencoding happening
                .arg("attached_pic")
                .arg(tools::get_path_as_string(&filename_cover))
                .output()
                .expect("Failed to execute process");

            if output.status.success() {
                fs::remove_file(filename).unwrap();
                return Some(filename_cover);
            } else {
                println!("{:?}", output.stderr);
            }
        }
    }

    None
}

fn write_tags(filename: &Path, entry: &entry::Entry, feed: &feed::Feed) -> Option<PathBuf> {
    if let Some(filename_no_ext) = get_filename_no_ext(filename) {
        if let Some(ext) = get_file_ext(filename) {
            let filename_tags = format!("{}-tags.{}", filename_no_ext, ext);
            let path = dowload_dir();
            let filename_tags = path.join(filename_tags);
            let output = Command::new("ffmpeg")
                .arg("-i")
                .arg(tools::get_path_as_string(filename))
                .arg("-y") //Always overwrite file
                .arg("-codec")
                .arg("copy") // Copy only no reencoding happening
                .arg("-write_id3v2")
                .arg("1")
                .arg("-metadata")
                .arg(format!(r#"title={}"#, entry.title).as_str())
                .arg("-metadata")
                .arg(format!(r#"album={}"#, feed.name).as_str())
                .arg("-metadata")
                .arg(format!(r#"artist={}"#, feed.name).as_str())
                .arg("-metadata")
                .arg(r#"genre=Podcast"#)
                .arg(tools::get_path_as_string(&filename_tags))
                .output()
                .expect("Failed to execute process");

            if output.status.success() {
                return Some(filename_tags);
            } else {
                println!("{:?}", output.stderr);
            }
        }
    }

    None
}

fn dowload_dir() -> PathBuf {
    let root = tools::get_root_folder();
    root.join("download")
}

fn clean_filename(filename: &str) -> String {
    let fname: String = filename.into();

    let re = Regex::new(r"[<>:/\|?*\x00-\x1F]").unwrap();
    let fname = re.replace_all(&fname, "").into_owned();

    let fname: String = fname.replace('"', "");

    let re = Regex::new(r"^(CON|PRN|AUX|NUL|COM1|COM2|COM3|COM4|COM5|COM6|COM7|COM8|COM9|LPT1|LPT2|LPT3|LPT4|LPT5|LPT6|LPT7|LPT8|LPT9)(\..+)?$").unwrap();
    let fname = re.replace_all(&fname, "").into_owned();

    let fname = trim_whitespace(&fname);

    let re = Regex::new(r"\.$").unwrap();
    let fname = re.replace_all(&fname, "").into_owned();

    if fname.is_empty() {
        return String::from("_");
    }

    fname
}

fn trim_whitespace(s: &str) -> String {
    let words: Vec<_> = s.split_whitespace().collect();
    words.join(" ")
}

fn get_uri_filename(url: &reqwest::Url) -> Option<String> {
    if let Some(mut path_segments) = url.path_segments() {
        if let Some(last) = path_segments.next_back() {
            return Some(String::from(last));
        }
    }

    None
}

fn get_file_ext(path: &Path) -> Option<String> {
    if let Some(ext) = path.extension() {
        let osstr = ext.to_os_string();
        if let Ok(osstr) = osstr.into_string() {
            return Some(osstr);
        }
    }

    None
}

fn get_filename_no_ext(path: &Path) -> Option<String> {
    if let Some(fstem) = path.file_stem() {
        let osstr = fstem.to_os_string();
        if let Ok(osstr) = osstr.into_string() {
            return Some(osstr);
        }
    }

    None
}

fn get_exe(exe: &str) -> Option<PathBuf> {
    if let Ok(ex) = which(exe) {
        return Some(ex);
    }

    None
}
