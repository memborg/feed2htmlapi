use crate::download;
use crate::feed;
use actix_web::{http::header, web, Error, HttpResponse};
use chrono::Utc;
use std::io::prelude::*;
use std::fs::File;
use uuid::Uuid;

#[derive(Serialize, Deserialize)]
pub struct ErrorMsg {
    pub message: String,
}

#[derive(Serialize, Deserialize)]
pub struct Success {
    pub success: bool,
}

pub async fn get_feeds() -> Result<HttpResponse, Error> {
    match feed::get_feeds() {
        Some(feeds) => Ok(HttpResponse::Ok().json(feeds)),
        _ => Ok(HttpResponse::InternalServerError().json(ErrorMsg {
            message: format!("Failed - {}", Utc::now()),
        })),
    }
}

pub async fn get_feed_read(user_id: web::Path<String>) -> Result<HttpResponse, Error> {
    let user_id = Uuid::parse_str(&user_id).unwrap().hyphenated();

    let fr = feed::get_read_by_user(user_id);
    Ok(HttpResponse::Ok().json(fr))
}

pub async fn mark_read(
    user_id: web::Path<String>,
    visited: web::Json<feed::Visited>,
) -> Result<HttpResponse, Error> {
    let user_id = Uuid::parse_str(&user_id).unwrap().hyphenated();

    let _v = feed::Visited {
        visited: visited.visited.clone(),
        feed_id: visited.feed_id.clone(),
    };
    match feed::mark_visited(user_id, _v) {
        Some(feed) => Ok(HttpResponse::Ok().json(feed)),
        _ => Ok(HttpResponse::InternalServerError().json(ErrorMsg {
            message: format!("Failed - {}", Utc::now()),
        })),
    }
}

pub async fn clean_feeds_read(user_id: web::Path<String>) -> Result<HttpResponse, Error> {
    let user_id = Uuid::parse_str(&user_id).unwrap().hyphenated();

    match feed::clean_feeds_read(user_id) {
        Some(feed) => Ok(HttpResponse::Ok().json(feed)),
        _ => Ok(HttpResponse::InternalServerError().json(ErrorMsg {
            message: format!("Failed - {}", Utc::now()),
        })),
    }
}

pub async fn get_feed(feed_id: web::Path<String>) -> Result<HttpResponse, Error> {
    match feed::get_feed(feed_id.to_string()) {
        Some(feed) => Ok(HttpResponse::Ok().json(feed)),
        _ => Ok(HttpResponse::InternalServerError().json(ErrorMsg {
            message: format!("Failed - {}", Utc::now()),
        })),
    }
}

pub async fn create_feed(feed: web::Json<feed::Feed>) -> Result<HttpResponse, Error> {
    let _feed = feed::Feed {
        id: String::new(),
        name: feed.name.clone(),
        link: feed.link.clone(),
        feed_type: feed.feed_type,
        clean_buffer: feed.clean_buffer,
        updated: String::new(),
        last_run: String::new(),
        tags: feed.tags.clone(),
    };
    match feed::create_feed(_feed) {
        Some(id) => match feed::get_feed(id) {
            Some(feed) => Ok(HttpResponse::Ok().json(feed)),
            _ => Ok(HttpResponse::InternalServerError().json(ErrorMsg {
                message: format!("Failed - {}", Utc::now()),
            })),
        },
        _ => Ok(HttpResponse::InternalServerError().json(ErrorMsg {
            message: format!("Failed - {}", Utc::now()),
        })),
    }
}

pub async fn update_feed(_feed: web::Json<feed::Feed>) -> Result<HttpResponse, Error> {
    Ok(HttpResponse::Ok().body(String::new()))
}

pub async fn delete_feed(feed_id: web::Path<String>) -> Result<HttpResponse, Error> {
    match feed::delete_feed(feed_id.to_string()) {
        Some(_) => Ok(HttpResponse::Ok().json(Success { success: true })),
        _ => Ok(HttpResponse::InternalServerError().json(ErrorMsg {
            message: format!("Failed - {}", Utc::now()),
        })),
    }
}

pub async fn download_entry(entry_id: web::Path<String>) -> HttpResponse {
    if let Some(path) = download::download_entry(entry_id.to_string()).await {
        let mut file_name = String::from("file.mkv");
        if let Some(filen) = path.file_name() {
            if let Some(fname) = filen.to_os_string().as_os_str().to_str() {
                file_name = fname.to_string();
            }

            let file_name = format!(
                r#"attachment; filename="{}" filename*=UTF-8''{}"#,
                file_name.clone(),
                file_name.clone()
            );

            let mut f = File::open(path).unwrap();
            let mut buffer = Vec::new();

            f.read_to_end(&mut buffer).unwrap();

            return HttpResponse::Ok()
                .append_header((header::PRAGMA, header::HeaderValue::from_static("public")))
                .append_header((header::CONTENT_TYPE, "application/octet-stream"))
                .append_header((
                        header::CONTENT_LENGTH,
                        header::HeaderValue::from_str(&buffer.len().to_string()).unwrap(),
                ))
                .append_header(("Content-Transfer-Encoding", "binary"))
                .append_header((
                        header::CONTENT_DISPOSITION,
                        header::HeaderValue::from_str(&file_name.clone()).unwrap(),
                ))
                .body(buffer);
        }
    }

    HttpResponse::NotFound().finish()
}
