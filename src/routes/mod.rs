use crate::handlers;
use actix_web::web;

pub fn routes(app: &mut web::ServiceConfig) {
    app.service(
        web::scope("/api")
            .route("/feeds", web::get().to(handlers::get_feeds))
            .route("/feeds", web::post().to(handlers::create_feed))
            .route("/feeds/{feedid}", web::get().to(handlers::get_feed))
            .route(
                "/entries/{entryid}/download",
                web::get().to(handlers::download_entry),
            )
            .route("/feeds/{feedid}", web::put().to(handlers::update_feed))
            .route("/feeds/{feedid}", web::delete().to(handlers::delete_feed))
            .route(
                "/feeds/read/{user_id}",
                web::get().to(handlers::get_feed_read),
            )
            .route("/feeds/read/{user_id}", web::post().to(handlers::mark_read))
            .route(
                "/feeds/read/{user_id}",
                web::delete().to(handlers::clean_feeds_read),
            ),
    );
}
